FROM openjdk:9
VOLUME /temp
ARG JAR_FILE
COPY de.ffischer.server/build/docker/de.ffischer.server-1.0-SNAPSHOT.jar app.jar
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","app.jar"]
