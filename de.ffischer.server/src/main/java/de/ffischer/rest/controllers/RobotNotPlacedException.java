package de.ffischer.rest.controllers;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.CONFLICT)
public class RobotNotPlacedException extends RuntimeException{

    static final String docPATH = "/swagger-ui.html#!/robot45controller/placeRobotUsingGET";

    public RobotNotPlacedException(String server) {
        super("The Robot is not placed at the tabletop. you HAVE TO place your robot before you can move it! please look at the docs:\n"+server+docPATH);
    }
}
