package de.ffischer.rest.controllers;

import de.ffischer.robot.*;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;



@RestController
@RequestMapping("/robot")
public class RobotController {
    @Value("${management.address}:${server.port}")
    String serverPort;
    ToyRobot robot;
    Game game;

    @ApiOperation(value =
            "That's the initial position at the tabletop to start with your robot.\n " +
            "The robot will always be look at North. In case of invalid values, the robot will start at x=0,y=0", response = Report.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "In case of invalid numbers (<0 or >size), the robot will start at x=0,y=0. So I guarantee you a successful game;)"),
            @ApiResponse(code=400,message = "Will be thrown by invalid values e.g. strings/decimal numbers instead of Ints")
    })
    //TODO:Strings as arguments has a ugly spring message!!
    @RequestMapping(method = RequestMethod.GET,value="/place")
    Report placeRobot(@RequestParam int x, @RequestParam int y) throws ToyRobotException {
        robot = new ToyRobot();
        game = new Game(new SquareBoard(4,4),robot);

        if(!game.placeToyRobot(new Position(x,y,Direction.NORTH))){
            game.placeToyRobot(new Position(1,1,Direction.NORTH));
            return new Report(0,0,Direction.NORTH);
        }
        return new Report(x,y,Direction.NORTH);
    }

    @ApiOperation(value = "Moves the robot one step in the direction")
    @ApiResponses(value = {
                    @ApiResponse(code=200, message = "In case of an invalid step the operation will be aborted and the response will be the current place of your robot. So yes, an aborted operation is indicated by a robot which has the same position after an operation"),
                    @ApiResponse(code=400, message = "Have you placed your robot?")

    })
    @RequestMapping(method = RequestMethod.POST,value="/move")
    Report moveRobot() throws ToyRobotException {
        if(robot != null){
            game.moveToyRobot();
            return new Report(robot.getPosition());
        }
        throw new RobotNotPlacedException(serverPort);

    }

    @ApiResponses(value = {
            @ApiResponse(code=400, message = "Please run /robot/place before you do this operation")
    })
    @ApiOperation(value = "Rotates the robot 90 degrees LEFT")
    @RequestMapping(method = RequestMethod.POST, value = "/rotate/left")
    Report rotateLeft() {
        if(robot != null){
            if(robot.rotateLeft()){
                return new Report(robot.getPosition());
            }
        }
        throw new RobotNotPlacedException(serverPort);
    }

    @ApiResponses(value = {
            @ApiResponse(code=400, message = "Please run /robot/place before you do this operation")
    })
    @ApiOperation(value = "Rotates the robot 90 degrees RIGHT")
    @RequestMapping(method = RequestMethod.POST, value = "/rotate/right")
    Report rotateRight() {
        if(robot != null){
            if(robot.rotateRight()){
                return new Report(robot.getPosition());
            }
        }
        throw new RobotNotPlacedException(serverPort);
    }

}
