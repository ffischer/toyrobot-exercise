# README
A small robot spring-rest-server for an job application - have fun ;)
Uses the new package-system from java 9 - so you really need java9.

* server runs at localhost:8080
* api:
    * docs: localhost:8080/swagger-ui.html
	* tests: de.ffischer.server/tests (post-/newman)
* requirements
    * java9
	* gradle 4.2.1
	
* docker
    * build / run
        * sudo docker build . # use the dockerfile & at the end you get an hash
        * sudo docker run -p 8081:8080 -t <hash_id> #host:container
    * commit / push
        * sudo docker login
        * sudo docker ps #container-id kopieren
        * sudo docker tag <<image_name>> <<docker_account/your_readable_name>>


