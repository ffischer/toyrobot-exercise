package de.ffischer.robot;


public class Report{
    private int x,y;

    public Report(Position pos){
        x = pos.x;
        y = pos.y;
        direction = pos.direction;
    }

    public Report(int x, int y, Direction direction) {
        this.x = x;
        this.y = y;
        this.direction = direction;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public Direction getDirection() {
        return direction;
    }

    public void setDirection(Direction direction) {
        this.direction = direction;
    }

    private Direction direction;
}
