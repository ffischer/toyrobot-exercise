package de.ffischer.robot;

public enum Command {
    PLACE,
    MOVE,
    LEFT,
    RIGHT,
    REPORT
}
