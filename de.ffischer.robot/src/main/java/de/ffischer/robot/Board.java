package de.ffischer.robot;

public interface Board {

    public boolean isValidPosition(Position position);

}